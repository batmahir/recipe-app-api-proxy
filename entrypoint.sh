#!/bin/sh

# this to set error to screen, if any of the script below fail, and return error to the screen
set -e


envsubst < /etc/nginx/default.conf.tpl > /etc/nginx/conf.d/default.conf 

# run nginx to run with daemon off, this is recommendation for docker because docker advise to run the application on foreground instead of background (daemon)
# and allow nginx log to be print out in docker output
nginx -g 'daemon off;' 